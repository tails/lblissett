# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: tails-index-pt\n"
"POT-Creation-Date: 2014-05-25 11:15+0200\n"
"PO-Revision-Date: 2014-04-30 19:46-0300\n"
"Last-Translator: Tails Developers <amnesia@boum.org>\n"
"Language-Team: Portuguese <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Portuguese\n"

#. type: Content of: outside any tag (error?)
msgid "[[!meta title=\"Privacy for anyone anywhere\"]]"
msgstr "[[!meta title=\"Privacidade para todos, em todos os lugares\"]]"

#. type: Content of: <div><div><p>
msgid ""
"Tails is a [[!wikipedia desc=\"live operating system\" Live_USB]], that you "
"can start on almost any computer from a DVD, USB stick, or SD card.  It aims "
"at preserving your <strong>privacy</strong> and <strong>anonymity</strong>, "
"and helps you to:"
msgstr ""
"Tails é um [[!wikipedia desc=\"sistema operacional live\" Live_USB]], que "
"você pode usar em quase qualquer computador a partir de um DVD, de uma "
"memória USB ou de um cartão SD. Ele tem como objetivo preservar sua "
"<strong>privacidade</strong> e seu <strong>anonimato</strong>, e te auxilia "
"a:"

#. type: Content of: <div><div><ul><li>
msgid ""
"<strong>use the Internet anonymously</strong> and <strong>circumvent "
"censorship</strong>;"
msgstr ""
"<strong>usar a Internet de forma anônima</strong> e <strong>evitar censura</"
"strong>;"

#. type: Content of: <div><div><ul><li>
msgid ""
"all connections to the Internet are forced to go through [[the Tor network|"
"https://www.torproject.org/]];"
msgstr ""
"todas as conexões feitas à Internet são passam necessariamente [[pela rede "
"Tor|https://www.torproject.org/]];"

#. type: Content of: <div><div><ul><li>
msgid ""
"<strong>leave no trace</strong> on the computer you are using unless you ask "
"it explicitly;"
msgstr ""
"<strong>não deixar rastros</strong> no computador que você estiver "
"utilizando, a menos que você explicitamente queira que isso aconteça;"

#. type: Content of: <div><div><ul><li>
msgid ""
"<strong>use state-of-the-art cryptographic tools</strong> to encrypt your "
"files, emails and instant messaging."
msgstr ""
"<strong>usar ferramentas criptográficas do estado da arte</strong> para "
"criptografar seus arquivos, email e mensagens instantâneas."

#. type: Content of: <div><div><p>
msgid "[[Learn more about Tails.|about]]"
msgstr "[[Saiba mais sobre o Tails.|about]]"

#. type: Content of: <div><div><h1>
msgid "News"
msgstr "Notícias"

#. type: Content of: <div><div>
msgid ""
"[[!inline pages=\"news/* and !news/*/* and !news/discussion and "
"currentlang()\" show=\"2\" feeds=\"no\" archive=\"yes\"]]"
msgstr ""
"[[!inline pages=\"news/* and !news/*/* and !news/discussion and "
"currentlang()\" show=\"2\" feeds=\"no\" archive=\"yes\"]]"

#. type: Content of: <div><div><p>
msgid "See [[News]] for more."
msgstr "Veja [[Notícias|news]] para mais informações."

#. type: Content of: <div><div><h1>
msgid "Security"
msgstr "Segurança"

#. type: Content of: <div><div>
msgid ""
"[[!inline pages=\"security/* and !security/audits and !security/audits.* "
"and !security/audits/* and !security/*/* and !security/discussion and "
"currentlang()\" show=\"2\" feeds=\"no\" archive=\"yes\"]]"
msgstr ""
"[[!inline pages=\"security/* and !security/audits and !security/audits.* "
"and !security/audits/* and !security/*/* and !security/discussion and "
"currentlang()\" show=\"2\" feeds=\"no\" archive=\"yes\"]]"

#. type: Content of: <div><div><p>
msgid "See [[Security]] for more."
msgstr "Veja [[Segurança|Security]] para mais informações."

#. type: Content of: <div><div>
msgid "[[!img lib/debian.png link=\"no\"]]"
msgstr "[[!img lib/debian.png link=\"no\"]]"

#. type: Content of: <div><div><p>
msgid "Tails is built upon <a href=\"https://debian.org/\">Debian</a>."
msgstr ""
"Tails é desenvolvido a partir do <a href=\"https://debian.org/\">Debian</a>."

#. type: Content of: <div><div>
msgid "[[!img lib/free-software.png link=\"no\"]]"
msgstr "[[!img lib/free-software.png link=\"no\"]]"

#. type: Content of: <div><div><p>
msgid "Tails is [[Free Software|doc/about/license]]."
msgstr "Tails é [[Software Livre|doc/about/license]]."

#. type: Content of: <div><div>
msgid "[[!img lib/tor.png link=\"no\"]]"
msgstr "[[!img lib/tor.png link=\"no\"]]"

#. type: Content of: <div><div><p>
msgid ""
"Tails sends its traffic through <a href=\"https://torproject.org/\">Tor</a>."
msgstr ""
"Tails envia seu tráfego pelo <a href=\"https://torproject.org/\">Tor</a>."

#~ msgid "Free Software"
#~ msgstr "Software Livre"

#~ msgid "It helps you to:"
#~ msgstr "Ele ajuda você a:"

#~ msgid ""
#~ "<em>The Amnesic Incognito Live System</em> (<a href='http://en.wikipedia."
#~ "org/wiki/Live_CD'>Live CD</a>, <a href='http://en.wikipedia.org/wiki/"
#~ "Live_USB'>Live USB</a>) is aimed at preserving your privacy and anonymity:"
#~ msgstr ""
#~ "<em>amnesia</em> es un <em>Sistema Live</em> ([Live CD](http://es."
#~ "wikipedia.org/wiki/Live_CD), [Live USB](http://es.wikipedia.org/wiki/"
#~ "Live_USB)) destinada a preservar su privacidad y anonimato:"

#~ msgid "no trace is left on local storage devices unless explicitly asked."
#~ msgstr ""
#~ "no deja ningun rastro en los dispositivos de almacenamiento locales a "
#~ "menos que sea explicitamente solicitado."

#~ msgid "It has many other [[features]]."
#~ msgstr "Tiene muchas otras [[funcionalidades|features]]."
