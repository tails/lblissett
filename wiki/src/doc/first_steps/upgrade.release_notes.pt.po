# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2014-08-12 22:33+0200\n"
"PO-Revision-Date: 2014-07-30 19:25-0300\n"
"Last-Translator: Tails Developers <amnesia@boum.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#.  Note for translators: make sure that the translation of this string
#. integrates well with /inc/stable_i386_release_notes. 
#. type: Content of: outside any tag (error?)
msgid ""
"We recommend you to read the [[!inline pages=\"inc/stable_i386_release_notes"
"\" raw=\"yes\"]] for the latest version. They document all the changes in "
"this new version:"
msgstr ""
"Nós recomendamos que você leia as [[!inline pages=\"inc/"
"stable_i386_release_notes.pt\" raw=\"yes\"]] da a última versão. Elas "
"documentam todas as mudanças nesta nova versão:"

#. type: Content of: <ul><li>
msgid "new features"
msgstr "novas funcionalidades"

#. type: Content of: <ul><li>
msgid "bugs that were solved"
msgstr "bugs que foram resolvidos"

#. type: Content of: <ul><li>
msgid "known issues that have already been identified"
msgstr "problemas conhecidos que já foram identificados"
