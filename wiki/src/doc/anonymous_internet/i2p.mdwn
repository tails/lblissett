[[!meta title="Using I2P"]]

<div class="caution">

<strong>

There is an critical security hole in I2P, that can lead to full
de-anonymization. [[Do read about
it|security/Security_hole_in_I2P_0.9.13]] before using I2P in Tails
1.1 and earlier.

</strong>

</div>

[I2P](https://geti2p.net/) is an alternative anonymity network to Tor
which supports most common Internet activities like web browsing,
email, filesharing etc. Unlike Tor, whose main focus arguably is on
accessing sites from the "normal" Internet, I2P is more oriented
towards being a closed [[!wikipedia darknet]], separate from the
"normal" Internet. Any one running I2P can run an anonymous server, a
so called Eepsite, that is only accessible within I2P using the `.i2p`
top level domain (similar to `.onion` for Tor hidden services). For
instance, the I2P homepage can also be accessed through I2P via
<http://i2p-projekt.i2p>.

Starting I2P
============

*I2P* is not enabled by default when Tails starts. To start *I2P*, do the
following:

  1. Add the <span class="command">i2p</span> boot option to the <span
  class="application">boot menu</span>. For detailed instructions, see the
  documentation on [[using the <span class="application">boot
  menu</span>|first_steps/startup_options#boot_menu]].

  2. From the desktop, choose <span class="menuchoice">
       <span class="guimenu">Applications</span>&nbsp;▸
       <span class="guisubmenu">Internet</span>&nbsp;▸
       <span class="guimenuitem">i2p</span></span>.

Once started, the so called router console will open in Tor Browser,
which shows I2P's current status, links to many useful I2P resources
(forums, email, filesharing etc.) and offers the possibility to
shutdown I2P. I2P is integrated in the browser in such a way that all
`.i2p` addresses are accessed correctly through I2P while all other
addresses are handled by Tor, all at the same time.
